<!--SPDX-FileCopyrightText: 2021 Jason Yundt <swagfortress@gmail.com>
SPDX-License-Identifier: CC0-1.0
-->
# Random Int Range
Generate a random int between 0 and N. No [modulo bias](https://research.kudelskisecurity.com/2020/07/28/the-definitive-guide-to-modulo-bias-and-how-to-avoid-it/)! Don't use this for security related things. This is not a cryptographically secure random number generator.

## Copying
This repo should be [REUSE](https://reuse.software/) compliant. For full copying information, install the [reuse tool](https://github.com/fsfe/reuse-tool), and run
`$ reuse spdx`

## Pre-commit
If you're going to contribute to this project, then you may want to have your contributions automatically checked. To do so,

1. Make sure that [pre-commit](https://pre-commit.com/) is installed.
1. `cd` to the root of the repo.
1. Run `$ pre-commit install`.
