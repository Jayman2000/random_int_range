# Copyright © 2004 Ted Unangst and Todd Miller. All rights reserved.
#
# Copyright © 1996 David Mazieres <dm@uun.org>
# Copyright © 1998, 2000-2002, 2004-2005, 2007, 2010, 2012-2015 Todd C. Miller <Todd.Miller@courtesan.com>
# Copyright © 2004 Ted Unangst
# Copyright © 2008 Damien Miller <djm@openbsd.org>
# Copyright © 2008, 2010-2011, 2016-2017 Otto Moerbeek <otto@drijf.net>
# Copyright © 2013 Markus Friedl <markus@openbsd.org>
# Copyright © 2014 Bob Beck <beck@obtuse.com>
# Copyright © 2014 Brent Cook <bcook@openbsd.org>
# Copyright © 2014 Pawel Jakub Dawidek <pjd@FreeBSD.org>
# Copyright © 2014 Theo de Raadt <deraadt@openbsd.org>
# Copyright © 2015 Michael Felt <aixtools@gmail.com>
# Copyright © 2015 Guillem Jover <guillem@hadrons.org>

# SPDX-FileCopyrightText: 2021 Jason Yundt <swagfortress@gmail.com>
# SPDX-License-Identifier: LicenseRef-Arc4RandomUniform


# Based on src/arc4random_uniform.c from libbsd-0.11.3:
# <https://libbsd.freedesktop.org/wiki/>

# randi() will always return a value that's less than RANDI_UPPER_BOUND
const RANDI_UPPER_BOUND = 4294967296

# Calculate a uniformly distributed random number less than upper_bound
# avoiding "modulo bias".
#
# Uniformity is achieved by generating new random numbers until the one
# returned is outside the range [0, 2**32 % upper_bound).  This
# guarantees the selected random number will be inside
# [2**32 % upper_bound, 2**32) which maps back to [0, upper_bound)
# after reduction modulo upper_bound.

static func random(upper_bound: int):
	var random_number: int
	var minimum: int

	if upper_bound < 2:
		return 0

	minimum = RANDI_UPPER_BOUND % upper_bound


	# This could theoretically loop forever but each retry has
	# p > 0.5 (worst case, usually far better) of selecting a
	# number inside the range we need, so it should rarely need
	# to re-roll.

	while true:
		random_number = randi()
		if random_number >= minimum:
			break

	return random_number % upper_bound
